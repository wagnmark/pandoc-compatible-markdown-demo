#!/bin/bash
pandoc --toc --standalone --mathjax="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js" -f markdown -t html formula.md -o formula.html
pandoc --toc --standalone --mathjax="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js" -f markdown -t pdf formula.md -o formula.pdf
