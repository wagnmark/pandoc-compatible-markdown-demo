# This is a demonstration on how to use LaTeX Forumlas in Markdown

An equation could be used inline, i.e. $`R=\frac{U}{I}`$, but also standalone:

$`R=\frac{U}{I}`$

All right? Math code blocks are not avaliable in standard pandoc markdown:

```math
R=\frac{U}{I}
```

But maybe one could utilize a template, at least with `html`.

# References

[Gitlab flavored Markdown - Math](https://docs.gitlab.com/ee/user/markdown.html#math)
